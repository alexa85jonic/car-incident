package com.car.part.insurance.demo.dto;

import lombok.Data;

import java.util.List;

@Data
public class IncidentRequestDto {

    Long companyNumber;
    Long contractNumber;
    List<CarPartInfo> brokenCarParts;

    @Data
    public static class CarPartInfo {
         Long carPartId;
         Double wageCost;
         Double materialCost;
    }

}
