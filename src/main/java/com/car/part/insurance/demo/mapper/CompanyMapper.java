package com.car.part.insurance.demo.mapper;

import com.car.part.insurance.demo.dto.CompanyRequestDto;
import com.car.part.insurance.demo.persistance.Company;
import lombok.experimental.UtilityClass;

@UtilityClass
public class CompanyMapper {

    private Company mapCarPartDtoToCarPartPojo(CompanyRequestDto companyRequestDto){
        return Company.builder()
                .companyName(companyRequestDto.getCompanyName())
                .build();
    }


}
