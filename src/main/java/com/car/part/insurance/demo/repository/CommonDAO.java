package com.car.part.insurance.demo.repository;

import com.car.part.insurance.demo.persistance.BaseEntity;

import java.util.List;

public interface CommonDAO {

	BaseEntity<?> findById(Class<?> cl, Long id);
	

}
