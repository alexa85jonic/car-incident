package com.car.part.insurance.demo.service.impl;


import com.car.part.insurance.demo.dto.IncidentRequestDto;
import com.car.part.insurance.demo.dto.IncidentResponseDto;
import com.car.part.insurance.demo.exception.BussinessException;
import com.car.part.insurance.demo.mapper.IncidentMapper;
import com.car.part.insurance.demo.persistance.Incident;
import com.car.part.insurance.demo.repository.impl.CarPartDaoImpl;
import com.car.part.insurance.demo.repository.impl.CompanyDaoImpl;
import com.car.part.insurance.demo.repository.impl.IncidentDaoImpl;
import com.car.part.insurance.demo.service.IncidentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class IncidentServiceImpl implements IncidentService {


    @Autowired
    IncidentDaoImpl incidentDao;

    @Autowired
    CarPartDaoImpl carPartDao;

    @Autowired
    CompanyDaoImpl companyDao;

    @Autowired
    IncidentMapper incidentMapper;


    @Override
    @Transactional
    public void saveCarPartIncident(IncidentRequestDto incidentRequestDto) {
        Incident incidentFromIncidentRequest = incidentMapper.getIncidentFromIncidentRequest(incidentRequestDto);
        incidentDao.saveOrUpdateIncident(incidentFromIncidentRequest);
    }

    @Override
    public IncidentResponseDto getCarPartIncident(Long incidentId) {
       return incidentMapper.getIncidentResponseDto(
               Optional.ofNullable(incidentDao.find(incidentId))
                       .orElseThrow(() -> new BussinessException("Incident with id : " + incidentId + " does not exist.")));
    }
}
