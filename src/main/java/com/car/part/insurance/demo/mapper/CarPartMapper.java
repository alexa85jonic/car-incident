package com.car.part.insurance.demo.mapper;

import com.car.part.insurance.demo.dto.CarPartRequestDto;
import com.car.part.insurance.demo.persistance.CarPart;
import lombok.experimental.UtilityClass;

@UtilityClass
public class CarPartMapper {

        private CarPart mapCarPartDtoToCarPartPojo(CarPartRequestDto carPartRequestDto){
            return CarPart.builder()
                    .carPartName(carPartRequestDto.getCarPartName())
                    .build();
        }



}
