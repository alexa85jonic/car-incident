package com.car.part.insurance.demo.dto;

import lombok.Data;

@Data
public class CarPartRequestDto {

    private Long carPartId;
    private String carPartName;


}
