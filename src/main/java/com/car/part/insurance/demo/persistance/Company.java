package com.car.part.insurance.demo.persistance;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "COMPANY")
public class Company extends BaseEntity<Long>{

    @Column(name = "company_name")
    private String companyName;

    @Builder
    public Company(Long id, Date created, Date updated, String companyName) {
        super(id, created, updated);
        this.companyName = companyName;
    }
}
