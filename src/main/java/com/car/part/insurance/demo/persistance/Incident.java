package com.car.part.insurance.demo.persistance;


import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "INCIDENT")
public class Incident extends BaseEntity<Long> {

    Double total;

    @ManyToOne
    Contract contact;
    @ManyToOne
    Company company;

    @OneToMany(mappedBy = "incident", fetch = FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    List<PartIncidentPrices> partIncidentPrices;

    @Builder
    public Incident(Long id, Date created, Date updated, Double total, Contract contact, List<PartIncidentPrices> partIncidentPrices,Company company) {
        super(id, created, updated);
        this.total = total;
        this.contact = contact;
        this.partIncidentPrices = partIncidentPrices;
        this.company = company;
    }
}
