package com.car.part.insurance.demo.persistance;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@MappedSuperclass
public abstract class BaseEntity<T> implements Serializable {

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		@SuppressWarnings("unchecked")
		BaseEntity<T> other = (BaseEntity<T>) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	private T id;
	
	private Date created;
	private Date updated;

	

	public T getId() {
		return id;
	}

	public void setId(T id) {
		this.id = id;
	}
	
	/**
	 * Returns true in case Hibernate has already persisted this entity
	 * an MySQL has provided identity value for {@#id} field, false
	 * otherwise meaning the entity is still transient.
	 * 
	 * @return 
	 */
	public boolean isPersistent() {
		return (id != null) ? true : false;
	}
	
	@PrePersist
	protected void onCreate() {
		created = new Date();
		updated = new Date();
	}

	@PreUpdate
	protected void onUpdate() {
		updated = new Date();
	}

	public Date getCreated() {
		return created;
	}

	public Date getUpdated() {
		return updated;
	}
		
}
