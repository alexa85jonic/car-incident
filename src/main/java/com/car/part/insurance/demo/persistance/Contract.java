package com.car.part.insurance.demo.persistance;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.Table;
import java.util.Date;


@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "CONTRACT")
public class Contract extends BaseEntity<Long> {

    @Column(name = "contract_name")
    private String contactName;

    @Builder
    public Contract(Long id, Date created, Date updated, String contactName) {
        super(id, created, updated);
        this.contactName = contactName;
    }
}
