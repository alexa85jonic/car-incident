package com.car.part.insurance.demo.persistance;


import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "CAR_PART")
public class CarPart extends BaseEntity<Long>{

    @Column(name = "carPartName")
    String carPartName;

    @OneToMany(mappedBy = "carPart")
    List<PartIncidentPrices> partIncidentPrices;

    @Builder
    public CarPart(Long id, Date created, Date updated, String carPartName) {
        super(id, created, updated);
        this.carPartName = carPartName;
    }
}
