package com.car.part.insurance.demo.validation;

import com.car.part.insurance.demo.exception.BussinessException;

public abstract class AbstractValidator {


    protected static void validateMandatory(Object value, String parameterName){
        if(value == null){
            StringBuilder sb = new StringBuilder();
            sb.append("Property " + parameterName + " is mandatory. ");
            throw new BussinessException(sb.toString());
        }
    }

    protected static void validateIfExist(Object value, String parameter)  {
        if(value == null){
            StringBuilder sb = new StringBuilder();
            sb.append(" does not exist in system. ");
            throw new BussinessException(sb.toString());
        }
    }

    protected static void validateAboveZeroNumericValue(Number value,String parameterName){
        if(value != null && value.doubleValue() < 0){
            StringBuilder sb = new StringBuilder();
            sb.append("Property " + parameterName + " can't be negative value. ");
            throw new BussinessException(sb.toString());
        }
    }
}
