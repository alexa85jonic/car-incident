package com.car.part.insurance.demo.repository.impl;

import com.car.part.insurance.demo.persistance.PartIncidentPrices;
import org.springframework.stereotype.Repository;

@Repository
public class PartIncidentDaoImpl extends GenericDAOImpl<PartIncidentPrices> {

    public PartIncidentPrices saveOrUpdatePart(PartIncidentPrices partIncidentPrices) {
        if (partIncidentPrices.getId() == null) {
            return create(partIncidentPrices);
        } else {
            return update(partIncidentPrices);
        }
    }


}




