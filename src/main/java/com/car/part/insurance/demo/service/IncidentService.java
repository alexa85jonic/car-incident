package com.car.part.insurance.demo.service;

import com.car.part.insurance.demo.dto.IncidentRequestDto;
import com.car.part.insurance.demo.dto.IncidentResponseDto;

public interface IncidentService {

    public void saveCarPartIncident(IncidentRequestDto incidentRequestDto);
    public IncidentResponseDto getCarPartIncident(Long incidentId);


}
