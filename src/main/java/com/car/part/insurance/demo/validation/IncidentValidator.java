package com.car.part.insurance.demo.validation;

import com.car.part.insurance.demo.dto.IncidentRequestDto;
import com.car.part.insurance.demo.repository.impl.CarPartDaoImpl;
import com.car.part.insurance.demo.repository.impl.CompanyDaoImpl;
import com.car.part.insurance.demo.repository.impl.ContractDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class IncidentValidator extends AbstractValidator{

    @Autowired
    CompanyDaoImpl companyDao;

    @Autowired
    ContractDaoImpl contactDao;

    @Autowired
    CarPartDaoImpl carPartDao;

    public void validateIncident(IncidentRequestDto incidentRequestDto){
        validateMandatory(incidentRequestDto.getCompanyNumber(),"companyNumber");
        validateMandatory(incidentRequestDto.getContractNumber(),"contractNumber");
        validateMandatory(incidentRequestDto.getBrokenCarParts(),"brokenCarParts");
        incidentRequestDto.getBrokenCarParts().forEach(e -> {
            validateMandatory(e.getCarPartId(),"carPartId");
            validateMandatory(e.getMaterialCost(),"materialCost");
            validateMandatory(e.getWageCost(),"wageCost");
            validateAboveZeroNumericValue(e.getMaterialCost(),"materialCost");
            validateAboveZeroNumericValue(e.getWageCost(),"wageCost");
        });

        validateIfExist(companyDao.find(incidentRequestDto.getCompanyNumber()),"companyNumber".concat(" ").concat(String.valueOf(incidentRequestDto.getCompanyNumber())));
        validateIfExist(contactDao.find(incidentRequestDto.getContractNumber()),"contactNumber".concat(" ").concat(String.valueOf(incidentRequestDto.getContractNumber())));
        incidentRequestDto.getBrokenCarParts().forEach(e ->validateIfExist(carPartDao.find(e.getCarPartId()),"carPartId".concat(" ").concat(String.valueOf(e.getCarPartId()))));
    }




}
