package com.car.part.insurance.demo.repository.impl;

import com.car.part.insurance.demo.persistance.Incident;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class IncidentDaoImpl extends GenericDAOImpl<Incident>{

    @Autowired
    PartIncidentDaoImpl partIncidentDao;
    public Incident saveOrUpdateIncident(Incident incident){
        if (incident.getId() == null) {
            Incident createdIncident = create(incident);
            Optional.ofNullable(createdIncident.getPartIncidentPrices()).ifPresent(e -> {
                e.forEach(partIncident -> {
                            partIncident.setIncident(createdIncident);
                            partIncidentDao.saveOrUpdatePart(partIncident);
                        }
                );
            });
            return find(createdIncident.getId());
        }else{
            return update(incident);
        }
    }


}
