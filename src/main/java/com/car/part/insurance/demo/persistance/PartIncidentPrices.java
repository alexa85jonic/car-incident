package com.car.part.insurance.demo.persistance;


import com.car.part.insurance.demo.repository.impl.GenericDAOImpl;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "PART_INCIDENT_PRICES")
public class PartIncidentPrices extends BaseEntity<Long> {

    Double wageCost;
    Double materialCost;

    @ManyToOne
    Incident incident;

    @ManyToOne
    CarPart carPart;

    @Builder
    public PartIncidentPrices(Long id, Date created, Date updated, Double wageCost, Double materialCost, Incident incident, CarPart carPart) {
        super(id, created, updated);
        this.wageCost = wageCost;
        this.materialCost = materialCost;
        this.incident = incident;
        this.carPart = carPart;
    }
}
