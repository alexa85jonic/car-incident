package com.car.part.insurance.demo.repository;


public interface GenericDAO<T> {

    T create(T t);

    T find(Object id);

    T update(T t);
    

}