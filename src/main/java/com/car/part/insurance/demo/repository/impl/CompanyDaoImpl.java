package com.car.part.insurance.demo.repository.impl;

import com.car.part.insurance.demo.persistance.Company;
import org.springframework.stereotype.Repository;

@Repository
public class CompanyDaoImpl extends GenericDAOImpl<Company>{

    public Company saveOrUpdateCompany(Company company){
        if (company.getId() == null) {
           return create(company);
        }else{
            return update(company);
        }
    }


}
