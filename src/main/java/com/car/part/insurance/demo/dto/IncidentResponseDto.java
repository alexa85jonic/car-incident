package com.car.part.insurance.demo.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class IncidentResponseDto {

    private Long claimNumber;
    private Long contactNumber;
    private Double total;
    private List<BrokenCarParts> brokenCarParts;

    @Data
    @Builder
    public static class BrokenCarParts {
        String carPartName;
        Double cost;
    }

}
