package com.car.part.insurance.demo.controller;

import com.car.part.insurance.demo.dto.IncidentRequestDto;
import com.car.part.insurance.demo.dto.IncidentResponseDto;
import com.car.part.insurance.demo.service.IncidentService;
import com.car.part.insurance.demo.validation.IncidentValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class CarPartIncidentController {

    @Autowired
    IncidentValidator incidentValidator;
    @Autowired
    IncidentService incidentService;

    @RequestMapping(value = "/incident",method = RequestMethod.POST)
    public ResponseEntity saveIncident(@RequestBody IncidentRequestDto requestDto){
        incidentValidator.validateIncident(requestDto);
        incidentService.saveCarPartIncident(requestDto);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/incident/{id}",method = RequestMethod.GET)
    public ResponseEntity saveIncident(@PathVariable ("id") Long id){
        IncidentResponseDto carPartIncident = incidentService.getCarPartIncident(id);
        return ResponseEntity.ok(carPartIncident);
    }


}
