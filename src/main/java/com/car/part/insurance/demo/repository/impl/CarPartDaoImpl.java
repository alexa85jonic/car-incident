package com.car.part.insurance.demo.repository.impl;

import com.car.part.insurance.demo.persistance.CarPart;
import com.car.part.insurance.demo.persistance.Contact;
import org.springframework.stereotype.Repository;

@Repository
public class CarPartDaoImpl extends GenericDAOImpl<CarPart>{

    public CarPart saveOrUpdateCarPart(CarPart carPart){
        if (carPart.getId() == null) {
           return create(carPart);
        }else{
            return update(carPart);
        }
    }


}
