package com.car.part.insurance.demo.repository.impl;

import com.car.part.insurance.demo.persistance.Contract;
import org.springframework.stereotype.Repository;

@Repository
public class ContractDaoImpl extends GenericDAOImpl<Contract>{

    public Contract saveOrUpdateContract(Contract contact){
        if (contact.getId() == null) {
           return create(contact);
        }else{
            return update(contact);
        }
    }


}
