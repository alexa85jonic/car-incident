package com.car.part.insurance.demo.mapper;

import com.car.part.insurance.demo.dto.ContractRequestDto;
import com.car.part.insurance.demo.persistance.Contract;
import lombok.experimental.UtilityClass;

@UtilityClass
public class ContactMapper {

    private Contract mapCarPartDtoToCarPartPojo(ContractRequestDto contractRequestDto){
        return Contract.builder()
                .contactName(contractRequestDto.getContactName())
                .build();
    }


}
